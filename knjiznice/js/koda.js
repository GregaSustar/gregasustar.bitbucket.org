/*global $, L, btoa, Chart, distance*/
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
 var ehrId = "";
 var pacient = {ime: "", priimek: "", spol: "", datumRojstva: "", starost:"", aktivnost: ""};
 if(stPacienta == 1) {
    pacient.ime = "Lenart";
    pacient.priimek = "Bonelli";
    pacient.spol = "Moški";
    pacient.datumRojstva = "1988-07-28";
    pacient.starost =  izračunajStarost(pacient.datumRojstva);
    pacient.aktivnost = "Telovadi 3-krat do 5-krat na teden";
    var datumInUra = "2019-04-16T11:00Z";
    var telesnaVisina = 190;
    var telesnaTeza = 95;
    var datumInUra2 = "2019-03-18T11:00Z";
    var telesnaVisina2 = 190;
    var telesnaTeza2 = 90;
 } else if (stPacienta == 2 ) {
    pacient.ime = "Saša";
    pacient.priimek = "Podgoršek";
    pacient.spol = "Ženska";
    pacient.datumRojstva = "1967-09-09";
    pacient.starost =  izračunajStarost(pacient.datumRojstva);
    pacient.aktivnost = "Ne Telovadi";
    datumInUra = "2019-04-16T12:00Z";
    telesnaVisina = 173;
    telesnaTeza = 60;
    datumInUra2 = "2019-03-18T12:00Z";
    telesnaVisina2 = 172;
    telesnaTeza2 = 63;
 } else if (stPacienta == 3) {
    pacient.ime = "Ian";
    pacient.priimek = "Spiller";
    pacient.spol = "Moški";
    pacient.datumRojstva = "2000-05-16";
    pacient.starost =  izračunajStarost(pacient.datumRojstva);
    pacient.aktivnost = "Telovadi več kot 5-krat na teden";
    datumInUra = "2019-04-14T13:00Z";
    telesnaVisina = 185;
    telesnaTeza = 85;
    datumInUra2 = "2019-03-18T13:00Z";
    telesnaVisina2 = 190;
    telesnaTeza2 = 87;
 }
 $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (data) {
        var ehrId = data.ehrId;
        //$("#header").html("EHR: " + ehrId);
    
        // build party data
        var partyData = {
            firstNames: pacient.ime,
            lastNames: pacient.priimek,
            dateOfBirth: pacient.datumRojstva + "T00:00Z",
            partyAdditionalInfo: [
                    {
                    key: "ehrId",
                    value: ehrId,
                    },
                    {
                    key: "spol",
                    value: pacient.spol,
                    },
                    {
                    key: "activity",
                    value:  pacient.aktivnost,
                    },
                    {
                    key: "starost",
                    value: pacient.starost
                    }
            ]
        };
        $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            headers: {
                "Authorization": getAuthorization()
            },
            success: function (party) {
                if (party.action == 'CREATE') {
                    // $("#header").html("Created: " + party.meta.href);
                    //pošiljanje ehrId na index
                    $("#izberiPacienta [id='" + stPacienta + "']").val($("#izberiPacienta [id='" + stPacienta + "']").val() +","+ ehrId);
                    
                    zapišiVitalne(datumInUra, telesnaVisina, telesnaTeza, ehrId);
                    zapišiVitalne(datumInUra2, telesnaVisina2, telesnaTeza2, ehrId);
                }
            }
        });
    }
});

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
//https://developer.nrel.gov/api/alt-fuel-stations/v1/nearest.json?api_key=9yJckwWOsiMayzZcsMbWje29ZUvJZg0eWWYAFF77&location=Denver+CO ---> baza za hrano

//--------------------------------------Aplikacija----------------------------------------------

function izračunajStarost(datumRojstvaS) {
    var today = new Date();
    
    var dd = today.getDate();
    var mm = today.getMonth();
    var yyyy = today.getFullYear();
    
    var letoRojstva = parseInt(datumRojstvaS.substring(0, 5), 10);
    var mesecRojstva = parseInt(datumRojstvaS.substring(5, 8), 10);
    var danRojstva = parseInt(datumRojstvaS.substring(8, 11), 10);
    
    if(mm+1 < mesecRojstva)
        var letoS = yyyy - letoRojstva - 1;
    else if(mm+1 == mesecRojstva && dd < danRojstva)
        letoS = yyyy - letoRojstva - 1;
    else
        letoS = yyyy - letoRojstva;

    return letoS;
}

function preveriSpol() {
    if($("[value='moški']").is(':checked'))
        return "Moški";
    else if ($("[value='ženska']").is(':checked'))
        return "Ženska";
}

function dodajVitalneZnakeRočno() {
    var ehrId = $("#vpisiEHRid").val();
    var datumInUra = $("#kreirajDatumInUro").val();
    var telesnaVisina = $("#kreirajVišino").val();
    var telesnaTeza = $("#kreirajTežo").val();
    
    if (!ehrId || ehrId.trim().length == 0 || !datumInUra || datumInUra.trim().length == 0 || !telesnaVisina || telesnaVisina.trim().length == 0 ||
        !telesnaTeza || telesnaTeza.trim().length == 0) {
		$("#KreirajSporociloVit").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosimo pravilno izpolnite vsa polja!</span>");
	} else {
        var podatki = {
    	// Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": datumInUra,
        "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
        "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    	};
    	var parametriZahteve = {
    	    ehrId: ehrId,
    	    templateId: 'Vital Signs',
    	    format: 'FLAT',
    	    //committer: merilec
    	};
    	$.ajax({
          url: baseUrl + "/composition?" + $.param(parametriZahteve),
          type: 'POST',
          contentType: 'application/json',
          data: JSON.stringify(podatki),
          headers: {
            "Authorization": getAuthorization()
          },
          success: function (res) {
            $("#KreirajSporociloVit").html(
              "<span class='obvestilo label label-success fade-in'>" +
              "Uspešno dodane meritve" + ".</span>");
            }
    	});
	}
}

function dodajEHRidRočno() {
    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val();
    var spol = preveriSpol();
    var aktivnost = $("#kreirajAktivnost").val();
    var starost = izračunajStarost(datumRojstva);
    
    if(!ime ||  ime.trim().length == 0 || !priimek ||  priimek.trim().length == 0 || !datumRojstva ||  datumRojstva.trim().length == 0 ||
        !spol ||  spol.trim().length == 0 || !aktivnost ||  aktivnost.trim().length == 0) {
        
        $("#result").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosimo izpolnite vsa polja!</span>");
    } else {
        $("#result").html("");
        $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        headers: {
            "Authorization": getAuthorization()
        },
        success: function (data) {
            var ehrId = data.ehrId;
    
            // build party data
            var partyData = {
                firstNames: ime,
                lastNames: priimek,
                dateOfBirth: datumRojstva + "T00:00Z",
                partyAdditionalInfo: [
                        {
                        key: "ehrId",
                        value: ehrId,
                        },
                        {
                        key: "spol",
                        value: spol,
                        },
                        {
                        key: "activity",
                        value:  aktivnost,
                        },
                        {
                        key: "starost",
                        value: starost
                        }
                ]
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                headers: {
                    "Authorization": getAuthorization()
                },
                success: function (party) {
                    if (party.action == 'CREATE') {
                        $("#result").html("<span class='obvestilo label label-success fade-in'>" +
                        "Kreiran EHR: " + ehrId + "</span>");
                    }
                }
            });
        }
        });
    }
}

function preberiZaObstoječega() {
    var ehrId = $("#preberiEHRid").val();
	var kcalAliBmi = $("#izbiraBranja").val();
    
	if (!ehrId || ehrId.trim().length == 0) {
		$("#KreirajSporociloPreberi").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite EHR id!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				//var starost = party.starost;
  				
  				    $.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/weight",
    			    type: 'GET',
    			    headers: {
                        "Authorization": getAuthorization()
                    },
    			    success: function (res1) {
    			            if(res1.length > 0 || !res1) {
            			        $.ajax({
                                url: baseUrl + "/view/" + ehrId + "/height",
                                type: 'GET',
                                headers: {
                                    "Authorization": getAuthorization()
                                },
                                success: function (res2) {
                                     var rezultat = "<table class='table table-hover'>" + "<tr><th class='text-left info'>Datum in ura</th>" +
                                                    "<th class='text-left info'>Telesna teža</th>" + "<th class='text-left info'> Telesna višina</th>" + "<th class='text-left info'>" + kcalAliBmi + "</th></tr>";
                                    if(kcalAliBmi == "BMI") {  
                                        for(var i in res2) {
                                            rezultat += "<td class='text-left'>" + res1[i].time +
                                              "</td><td class='text-left'>" + res1[i].weight + " " + res1[i].unit +
                                              "</td><td class='text-left'>"+ res2[i].height + " " + res2[i].unit +
                                              "</td><td class='text-left success fade-in'><b>"+ izračunajBMI(res1[i].weight, res2[i].height) + "<b></td></tr>";
                                        }
                                    } else {
                                        for(var i in res2) {
                                            rezultat += "<td class='text-left'>" + res1[i].time +
                                              "</td><td class='text-left'>" + res1[i].weight + " " + res1[i].unit +
                                              "</td><td class='text-left'>"+ res2[i].height + " " + res2[i].unit +
                                              "</td><td class='text-left success fade-in'><b>"+ izračunajKcal(res1[i].weight, res2[i].height, party.additionalInfo.starost, 
                                              party.additionalInfo.activity, party.additionalInfo.spol) + " kcal" + "<b></td></tr>";
                                        }
                                    }
                                    rezultat += "</table>";
                                    $("#rezultatiBranja").html(rezultat);
                                    $("#rezultatiBranja").addClass("panel");
                                    $("#KreirajSporociloPreberi").html("");
                                    $("#zaKogaBeremo").html("<div>Pacient: <b>" + party.firstNames + " " + party.lastNames + "</b></div>");
                                },
                                });
    			            } else {
    			       $("#KreirajSporociloPreberi").html("<span class='obvestilo " +
                            "label label-warning fade-in'>Ni podatkov!"); 
    			            }
    			    },
  				});
  			},
  			error: function() {
  			    $("#KreirajSporociloPreberi").html("<span class='obvestilo " +
                    "label label-danger fade-in'>EHR id ne obstaja!");
  			}
		});
	}
}

function zapišiVitalne(datumInUra, telesnaVisina, telesnaTeza, ehrId) {
    var podatki = {
    	// Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": datumInUra,
        "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
        "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    };
    var parametriZahteve = {
        ehrId: ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        //committer: merilec
    };
    $.ajax({
    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(podatki),
    headers: {
    "Authorization": getAuthorization()
    },
    success: function (res) {
    $("#result").html(
      "<span class='obvestilo label label-success fade-in'>" +
      "Pacienti so bili uspešno generirani" + ".</span>");
      
      // pošiljanje podatkov na index da jih lahko kasneje dostopamo (PROPADLA IDEJA)
      /*$("#izberiPacienta [id='" + stPacienta + "']").val(function(){
        var string = "";
        for(var i in pacient) {
            string = string + pacient[i] + ",";
        }
        string = string + datumInUra+","+ telesnaTeza +","+ telesnaVisina +","+ ehrId;
        return string;
    });*/
    },
    error: function(err) {
      $("#result").html(
      "<span class='obvestilo label label-danger fade-in'>Napaka '" +
      JSON.parse(err.responseText).userMessage + "'!");
    }
    });
}

function izračunajBMI(weight, height) {
    var heightInM = height/100;
    return (weight/(heightInM * heightInM)).toFixed(1);
}

function izračunajKcal(weight, height, age, activity, gender) {
    var stopnjaAktivnosti = -1;
    if(activity == "Ne Telovadi") stopnjaAktivnosti = 1;
    else if(activity == "Telovadi 1-krat do 2-krat na teden") stopnjaAktivnosti = 2;
    else if(activity == "Telovadi 3-krat do 5-krat na teden") stopnjaAktivnosti = 3;
    else if(activity == "Telovadi več kot 5-krat na teden") stopnjaAktivnosti = 5;
   //Men 10 x weight (kg) + 6.25 x height (cm) – 5 x age (y) + 5
   //Women 10 x weight (kg) + 6.25 x height (cm) – 5 x age (y) – 161.
    if(gender == "Moški") {
        return Math.round(10*weight + 6.25*height - 5*age + 5 + (200*stopnjaAktivnosti));
    } else {
        return Math.round(10*weight + 6.25*height - 5*age -161 + (200*stopnjaAktivnosti));
    }
}

var proteinGM = 0;
var fatGM = 0;
var carbsGM = 0;

function getHranilneVrednosti(foodId, callback) {
    $.ajax({
        url: "https://api.nal.usda.gov/ndb/nutrients/?format=json&api_key=NXcjOT0zJVJllOMfZkB9kzBHQHzajngMXV6JC2NA&nutrients=203&nutrients=204&nutrients=205&nutrients=208&ndbno=" + foodId,
        type: 'GET',
        dataType: "json",
        success: function(res) {
            //console.log(res);
            var energy = res.report.foods[0].nutrients[0];
            var protein = res.report.foods[0].nutrients[1];
            var fat = res.report.foods[0].nutrients[2];
            var carbs = res.report.foods[0].nutrients[3];
            proteinGM = protein.gm;
            fatGM = fat.gm;
            carbsGM = carbs.gm;
            
            var rezultat = "<td>"+ energy.gm + " "+ energy.unit +"</td><td>"+ 
                            protein.gm + " "+ protein.unit +"</td><td>"+ 
                            carbs.gm + " "+ carbs.unit +"</td><td>"+ 
                            fat.gm + " "+ fat.unit +"</td>";
                            
            $("#rezultatNutrition").html(rezultat);
            callback();
        },
        error: function() {
            $("#rezultatNutrition").html("<tr><td>Prišlo je do napake!</td></tr>");
        }
    });
}

//graf
var myDoughnutChart;
function narisiGraf() {
    var ctx = document.getElementById('chart-area').getContext('2d');
	myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [proteinGM, fatGM, carbsGM, Math.round((100-proteinGM-fatGM-carbsGM)*100) / 100],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)'
                    ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)'
                    ],
                borderWidth: 1
            }],
            labels: ["Proteini", "Maščobe", "Ogljikovi hidrati", "Ostalo"],
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return data.labels[tooltipItems.index] + ': ' + data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index] + 'g';
                    }
                }
            },
            legend: {
                display: false
            }
        }
    });
}

// Kako zapravt 3 ure življenja z updatanjem k nč ne dela namest da bi sam vedno ustvaru nov graf pa prejšnga destroy-u (RIP 14:00-17:00 18.05.2019)

/*function addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

function removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}*/

// klicanje getHranilneVrednosti

function handler( event ) {
    var target = $( event.target );
    if ( target.is( ".list-group-item" ) ) {
        var foodId = target.attr("value");
        getHranilneVrednosti(foodId, function() {
            myDoughnutChart.destroy();
            narisiGraf();
        });
    }
}
$(document).click( handler );

$(document).ready(function() {
    // samo izpolnjevanje obrazcev ob izbiri generiranega pacienta
    $("#izberiPacienta").click(function() {
       $("#kreirajSporocilo").html("");
        var podatki = $(this).val().split(",");
        
        $("#kreirajIme").val(podatki[0]);
        $("#kreirajPriimek").val(podatki[1]);
        
        if(podatki[2] == "Moški")
            $("[value='moški']").prop("checked", true);
        else if (podatki[2] == "Ženska")
            $("[value='ženska']").prop("checked", true);
        
        $("#kreirajDatumRojstva").val(podatki[3]);
        $("#kreirajAktivnost").val(podatki[5]);
        
        $("#vpisiEHRid").val(podatki[9]);
        $("#kreirajDatumInUro").val(podatki[6]);
        $("#kreirajTežo").val(podatki[7]);
        $("#kreirajVišino").val(podatki[8]);
    });
    narisiGraf();
});

//--------------------------------Bolnišnice----------------------------------------

var map;
var centriPoligonov2 = [];
const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

function narisiBolnice() {
    $.getJSON( "/knjiznice/json/bolnisnice.json", function(data) {
            var tabela = data.features;
            var znacilnosti = [];
            var koordinatePoligonov = [];
            
            tabela.forEach(function(item, index){
                znacilnosti.push(item.properties);
            });
            
            tabela.forEach(function(item, index) {
                koordinatePoligonov.push(item.geometry.coordinates);
            });
            //console.log(koordinatePoligonov);
            koordinatePoligonov.forEach(function(item, index) {
                if(item != null) {
                    //console.log(item);
                    item.forEach(function(item2, index2) {
                        if(item2 != null && item2.length == 2) {
                            // to so use tocke
                            //console.log(item2);
                            item2.reverse();
                            //console.log(item2);
                            //var marker = L.marker(item2).addTo(map);
                        } else {
                            // to so usi poligoni
                            item2.forEach(function(item3, index3) {
                               //console.log(item3); 
                               item3.reverse();
                            });
                            
                            var polygon = L.polygon(item, {color: 'blue'}).addTo(map);
                            centriPoligonov2.push(polygon.getBounds().getCenter());
                            if(znacilnosti[index].name !== undefined) {
                                polygon.bindPopup("<b>" + znacilnosti[index].name + "</b>" + "<br>" + znacilnosti[index]["addr:street"] + " " + 
                                    znacilnosti[index]["addr:housenumber"] + ", " + znacilnosti[index]["addr:postcode"] + " " + znacilnosti[index]["addr:city"]);
                            }
                        }
                    });
                }
            }); 
        }
    );
}


    

window.addEventListener("load", function() {
    
    console.log("redy");
    // Osnovne lastnosti mape
    var mapOptions = {
        center: [FRI_LAT, FRI_LNG],
        zoom: 12
        // maxZoom: 3
    };

    // Ustvarimo objekt mapa
    map = new L.map('map', mapOptions);
    
    // Ustvarimo prikazni sloj mape
    var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    
    // Prikazni sloj dodamo na mapo
    map.addLayer(layer);
    
    narisiBolnice(); 
     
    /*var clickCircle;
    function onMapClick(e) {
        if (clickCircle != undefined) {
          map.removeLayer(clickCircle);
        }
        clickCircle = L.circle(e.latlng, {color: 'red', opacity: 0.5, fillColor: 'red', fillOpacity: 0.0, radius: 2000}).addTo(map);
        //console.log(centriPoligonov2);
        
        centriPoligonov2.forEach(function(item, index) {
           if(item.distanceTo(e.latlng) <= 1000) {
               //console.log(item);
           } 
        });
    }
    map.on('click', onMapClick);*/
});
